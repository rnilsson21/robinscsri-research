
import math
from typing import Tuple

from PIL import Image
photo = Image.open( "lilac-1024x768.jpg" )

def compute_shade_arithmetic_mean( color ):
    (red, green, blue) = color

    average = (red + green + blue) // 3

    return (average, average, average)
# compute_shade_arithmetic_mean()

def compute_shade_min_max_mean( color ):
    (red, green, blue) = color

    minimum = min( red, green, blue )
    maximum = max( red, green, blue )

    average = (minimum + maximum) // 2

    return (average, average, average)
# end of computer_shade_min_max_mean()

def compute_shade_weighted_average( color, weights ):
    (red, green, blue) = color
    (red_weight, green_weight, blue_weight) = weights

    average = int(red_weight * red + green_weight * green + blue_weight * blue)

    return (average, average, average)
# end of compute_shade_weighted_average()

def choose_color( color, i, j, width, height ):
    # TO-DO: experiment with other formulas
    # for deciding which pixels should be
    # drawn in color and which should be 
    # drawn in black & white

    # TO-DO experiment with each of the
    # following color-to-share-of-gray algorithms

    rule_width = 6
    black: Tuple[255, 255, 255] = (255, 255, 255)



    # color change of stop sign
    (r,g,b)= color
    if 20 < r < 60 and 5 < g < 20 and 10 < b < 25:
        return (221,245,24)
    # color change of cone
    if 100 < r < 200 and 30 < g < 45 and 30 < b < 40:
        return (225, 74, 112)
    #cone pt 2
    if 70 < r < 200 and 10 < g < 40 and 10 < b < 40:
        return (129,53,99)
    #cone pt 3
    if 80 < r < 300 and 20 < g < 60 and 20 < b < 60:
        return (136,92,113)
    # color change of clothes
    if 4 < r < 10 and 4 < g < 14 and 7 < b < 20:
        return (0,0,0)
    else:
        return color

    # color change of cone


# end of choose_color()

def color_to_black_and_white( photo ):
    (width, height) = photo.size


    copy = photo.copy()

    for i in range( width ):
        for j in range( height ):
            color = photo.getpixel( (i, j) )

            color = choose_color( color, i, j, width, height )

            copy.putpixel( (i, j), color )

    return copy
# end of color_to_black_and_white()


def main():
    photo = Image.open( "lilac-1024x768.jpg" )
    #photo.show()

    (width, height) = photo.size
    print( "width = ", width )
    print( "height = ", height )

    bw_photo = color_to_black_and_white( photo )

    bw_photo.save ("robinpic.jpg")
    photo.show()


if __name__ == "__main__":
    main()


